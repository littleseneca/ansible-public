# ansible-public
This is my collection of Ansible Roles and Playbooks. Feel free to use any of the content listed within

## Hashicorp-Vault
This role spins up a super basic hashicorp vault instance for rhel 7-8 devices (tested on centos 8, but should work for 7 as well). 

The role does not contain any security/redundancy features, like highly available storage or tls encryption. The assumption is that you would add whatever functions you deem important. 

## Kubernetes-Install
This project is the result of many hours of work, and is intended to perform all the tasks associated with standing up a fully functional Kuberenetes cluster. All you need to do is change the hosts listed within the inventory file to something that makes sense to you.

All = MUST contain all of the devices you want to use in your K8 Cluster
Prime_Control = MUST be the first node of the k8 cluster and the first member of the control plane
Controls = the nodes you want to be control planes
workers = the devices you want to be worker nodes

# Authors and acknowledgment
I'd like to thank Sander Van Vugt for his fantastic RHCE guide, which I am using as I build out these roles and projects. 

# License
All roles included within this repository are listed under GNU General Public License version 3. 

# Project status
This project is currently active, and will continually be updated until I have successfully completed my RHCE examination. 
