Role Name
=========

This role spins up a super basic hashicorp vault instance for rhel 7-8 devices (tested on centos 8, but should work for 7 as well). 

The role does not contain any security/redundancy features, like highly available storage or tls encryption. The assumption is that you would add whatever functions you deem important. 

Requirements
------------

This role is build to be 100% idempotent and does not rely on anything not included within the role. 

Role Variables
--------------

Variables included point to hashicorp repositories for RHEL based distributions. 

Dependencies
------------

This role has no dependencies

Example Playbook
----------------

To use this role, simply create an ansible playbook, list your hosts, and then define the hashicorp-vault role as the content of the playbook, as shown below:

    - hosts: servers
      roles:
         - hashicorp-vault

License
-------

GPL 3
