variable "pm_hypervisor_url" {
  type = string
  default = "https://yourIPhere:8006/api2/json"
}
variable "pm_target_node" {
  type = list
  default = ["Node1","Node2","Node3"]
}
variable "pm_target_storage" {
  type = list
  default = ["StorageSource1","StorageSource2","StorageSource3"]
}
variable "ip_range" {
  type = string
  default = "192.168"
}
variable "pm_api_token_id" {
  type = string
  default = "Your API Token ID"
}
variable "pm_api_token_secret" {
  type = string
  default = "Your API Token Secret"
}
variable "pm_template_name" {
  type = string
  default = "Your Template Name"
}
variable "pm_cloud_user" {
  type = string
  default = "administrator"
}
variable "pm_cloud_pass" {
  type = string
  default = "Your Cloud Password"
}
variable "pm_cloud_search" {
  type = string
  default = "Your Search Domain"
}
variable "tailscale_auth" {
  type = string
  default = "Your Tailscale Authentication Key"
}
