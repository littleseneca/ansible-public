#!/bin/bash
home=$(pwd)
cd ${home}/terraform
terraform init
terraform plan
terraform apply -auto-approve
cd ${home}/ansible
ansible-playbook main.yml 