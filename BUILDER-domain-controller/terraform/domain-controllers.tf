terraform {
  required_providers {
    proxmox = {
      source = "telmate/proxmox"
      version = "2.9.10"
    }
  }
}
provider "proxmox" {
  pm_api_url = var.pm_hypervisor_url
  pm_api_token_id = var.pm_api_token_id
  pm_api_token_secret = var.pm_api_token_secret
  pm_tls_insecure = true
}
resource "proxmox_vm_qemu" "domain_controllers" {
  vmid              = "100${count.index}"
  count             = 3
  name              = "dc${count.index}"
  target_node       = var.pm_target_node[count.index]
  clone             = var.pm_template_name
  os_type           = "cloud-init"
  cores             = 2
  sockets           = "1"
  cpu               = "host"
  memory            = 4098
  scsihw            = "virtio-scsi-pci"
  bootdisk          = "scsi0"
  agent             = 1
  disk {
    size            = "30G"
    type            = "scsi"
    storage         = var.pm_target_storage[count.index]
  }
  network {
    model           = "virtio"
    bridge          = "vmbr0"
  }
  lifecycle {
    ignore_changes  = [
      network,
    ]
  }
  # Cloud Init Settings
  ipconfig0 = "ip=${var.ip_range}.10.10${count.index}/16,gw=${var.ip_range}.0.1"
  nameserver = "1.1.1.1"
  ciuser = var.pm_cloud_user
  cipassword = var.pm_cloud_pass 
  sshkeys = <<EOF
${file("keys.pub")}
EOF


  # Domain Join Process
  connection {
    type = "ssh"
    user = var.pm_cloud_user
    private_key = "${file("YOUR KEY HERE")}"
    host = "${var.ip_range}.10.10${count.index}" 
  }
  provisioner "remote-exec" {
    inline = [
        "sudo dnf clean packages -y",
        "sudo dnf update -y",
        "sudo dnf config-manager --add-repo https://pkgs.tailscale.com/stable/centos/8/tailscale.repo -y",
        "sudo dnf install tailscale -y",
        "sudo systemctl enable --now tailscaled python3",
        "sudo tailscale up --authkey=${var.tailscale_auth}"
    ]
  }
}
