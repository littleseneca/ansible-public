## Kubernetes-Install
This project is the result of many hours of work, and is intended to perform all the tasks associated with standing up a fully functional Kuberenetes cluster. All you need to do is change the hosts listed within the inventory file to something that makes sense to you.

All = MUST contain all of the devices you want to use in your K8 Cluster
Prime_Control = MUST be the first node of the k8 cluster and the first member of the control plane
Controls = the nodes you want to be control planes
workers = the devices you want to be worker nodes

# Authors and acknowledgment
I'd like to thank Sander Van Vugt for his fantastic RHCE guide, which I am using as I build out these roles and projects. 

# License
All roles included within this repository are listed under GNU General Public License version 3. 

# Project status
This project is currently active, and will continually be updated until I have successfully completed my RHCE examination. 
